package com.user.priority.UserPriority.dto;

public class OutputResponseDto {

	private boolean success;
	private String statusCode;
	private Object data;
	private String message;
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public OutputResponseDto(boolean success, String statusCode, Object data, String message) {
		super();
		this.success = success;
		this.statusCode = statusCode;
		this.data = data;
		this.message = message;
	}
	public OutputResponseDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "OutputResponseDto [success=" + success + ", statusCode=" + statusCode + ", data=" + data + ", message="
				+ message + "]";
	}
}
