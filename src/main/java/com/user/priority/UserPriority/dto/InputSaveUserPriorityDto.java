package com.user.priority.UserPriority.dto;

import org.springframework.stereotype.Component;

@Component
public class InputSaveUserPriorityDto {

	private String sPriorityName;
	
	private Integer nRateOfSatisfaction;
	public String getsPriorityName() {
		return sPriorityName;
	}
	public void setsPriorityName(String sPriorityName) {
		this.sPriorityName = sPriorityName;
	}
	public Integer getnRateOfSatisfaction() {
		return nRateOfSatisfaction;
	}
	public void setnRateOfSatisfaction(Integer nRateOfSatisfaction) {
		this.nRateOfSatisfaction = nRateOfSatisfaction;
	}
	@Override
	public String toString() {
		return "InputSaveUserPriorityDto [sPriorityName=" + sPriorityName + ", nRateOfSatisfaction="
				+ nRateOfSatisfaction + "]";
	}
	
}
