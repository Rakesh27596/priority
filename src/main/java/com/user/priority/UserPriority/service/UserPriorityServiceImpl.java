package com.user.priority.UserPriority.service;

import java.util.List;
import java.util.function.Predicate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.user.priority.UserPriority.dto.InputSaveUserPriorityDto;
import com.user.priority.UserPriority.dto.OutputResponseDto;
import com.user.priority.UserPriority.model.Priority;
import com.user.priority.UserPriority.model.UserPriorityMapping;
import com.user.priority.UserPriority.repository.IPriorityRepository;
import com.user.priority.UserPriority.repository.IUserPriorityMappingRepository;


@Service
public class UserPriorityServiceImpl implements IUserPriorityService{

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	IPriorityRepository priorityRepository;
	
	@Autowired
	IUserPriorityMappingRepository userPriorityMappingRepository;
	
	public static Boolean checkRateOfSatisfaction(int nRateOfSatisfaction){  
        if(nRateOfSatisfaction > 0 && nRateOfSatisfaction < 6)  
            return true;  
        else 
        	return false;  
    }  
	
	@Override
	public OutputResponseDto saveUserPriority(InputSaveUserPriorityDto inputSaveUserPriorityDto) {
		logger.info("Inside save user priority service");

		// Checking whether area of priority and it's rate of satisfaction is null or not using predefined functional interface.
		Predicate<InputSaveUserPriorityDto> checkPriorityName = (
				inputDetails) -> inputDetails.getsPriorityName() == null || inputDetails.getsPriorityName() == "";

		Predicate<InputSaveUserPriorityDto> checkRateOfSatisfaction = (
				inputDetails) -> inputDetails.getnRateOfSatisfaction() == null;
				
		if (checkPriorityName.test(inputSaveUserPriorityDto)) {
			return new OutputResponseDto(false, "prd-400-005", null, "Area Of Priority Cannot Be Null Or Blank");
		}
		
		if (checkRateOfSatisfaction.test(inputSaveUserPriorityDto)) {
			return new OutputResponseDto(false, "prd-400-006", null, "Rate Of Satisfaction Of Priority Cannot Be Null");
		}

		// Checking whether rate of satisfaction is scaled between 1-5 or not using Method Reference.
        Predicate<Integer> predicate =  UserPriorityServiceImpl::checkRateOfSatisfaction;  
          
        boolean result = predicate.test(inputSaveUserPriorityDto.getnRateOfSatisfaction());  
        if(!result) {
        	return new OutputResponseDto(false, "prd-400-007", null, "Priorities Rate Of Satisfaction Should be scale between 1-5");
        }

		// Check whether the priority name exists or not.
		Priority priority = priorityRepository.findBySPriorityName(inputSaveUserPriorityDto.getsPriorityName());
		logger.info("Priority Details:-" + priority.getsPriorityName());
		if (priority != null) {
			// Saving User Prioriies Details.
			UserPriorityMapping userPriorityMapping = new UserPriorityMapping();
			userPriorityMapping.setnRateOfSatisfaction(inputSaveUserPriorityDto.getnRateOfSatisfaction());
			userPriorityMapping.setPriority(priority);
			logger.info("User Priority Details:-" + userPriorityMapping);
			userPriorityMappingRepository.save(userPriorityMapping);
			return new OutputResponseDto(true, "prd-400-004", null, "User Priority Saved Successfully.");
		} else {
			return new OutputResponseDto(false, "prd-400-003", null, "Please Enter Valid Area Of Priority");
		}

	}

	@Override
	public OutputResponseDto getPriorities() {
		logger.info("Get list of all priorities");
		
		// Finding all priorities.
		List<Priority> listOfPriorities = (List<Priority>) priorityRepository.findAll();
		if(!listOfPriorities.isEmpty()) {
			return new OutputResponseDto(true, "prd-200-001", listOfPriorities, "List Of Priorities Returned Successfully");
		} else {
			return new OutputResponseDto(false, "prd-400-002", null, "List Of Priorities Not Found");
		}
	}

}
