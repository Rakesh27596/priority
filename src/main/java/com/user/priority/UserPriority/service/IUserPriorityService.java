package com.user.priority.UserPriority.service;

import com.user.priority.UserPriority.dto.InputSaveUserPriorityDto;
import com.user.priority.UserPriority.dto.OutputResponseDto;

public interface IUserPriorityService {
	
	public OutputResponseDto getPriorities();
	
	public OutputResponseDto saveUserPriority(InputSaveUserPriorityDto inputSaveUserPriorityDto);
}
