package com.user.priority.UserPriority.controller;




import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.user.priority.UserPriority.dto.InputSaveUserPriorityDto;
import com.user.priority.UserPriority.dto.OutputResponseDto;
import com.user.priority.UserPriority.service.UserPriorityServiceImpl;

@RestController
@CrossOrigin
public class UserPriorityController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	UserPriorityServiceImpl userPriorityServiceImpl;
	
	@RequestMapping(value = "/getPriorities", method = RequestMethod.GET)
	public OutputResponseDto getPriorities() {
		logger.info("Get Priorities");
		
		OutputResponseDto outputResponseDto = userPriorityServiceImpl.getPriorities();
		return outputResponseDto;
	}
	
	@RequestMapping(value = "/save/priority", method = RequestMethod.POST)
	public OutputResponseDto saveUserPriority(@RequestBody @Valid InputSaveUserPriorityDto inputSaveUserPriorityDto) {
		logger.info("Parmeters to save user priorities:-"+inputSaveUserPriorityDto);
		
		OutputResponseDto outputResponseDto = userPriorityServiceImpl.saveUserPriority(inputSaveUserPriorityDto);
		return outputResponseDto;
	}
	
}
