package com.user.priority.UserPriority.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.user.priority.UserPriority.model.Priority;

@Repository
public interface IPriorityRepository extends CrudRepository<Priority, Integer>{

	@Query(value = "select * from priority where priority_name=:sPriorityName", nativeQuery = true)
	public Priority findBySPriorityName(@Param("sPriorityName") String sPriorityName);

	
}
