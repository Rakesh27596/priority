package com.user.priority.UserPriority.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.user.priority.UserPriority.model.UserPriorityMapping;

@Repository
public interface IUserPriorityMappingRepository extends CrudRepository<UserPriorityMapping, Integer>{

	
}
