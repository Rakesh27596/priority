package com.user.priority.UserPriority.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="priority")
@NamedQuery(name="Priority.findAll", query="SELECT p from Priority p")
public class Priority implements Serializable{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="priority_id")
	private Integer nPriorityId;
	
	@Column(name="priority_name")
	private String sPriorityName;
	
	@OneToMany(mappedBy = "priority")
	private List<UserPriorityMapping> userPriorityMappings;

	public Integer getnPriorityId() {
		return nPriorityId;
	}

	public void setnPriorityId(Integer nPriorityId) {
		this.nPriorityId = nPriorityId;
	}

	public String getsPriorityName() {
		return sPriorityName;
	}

	public void setsPriorityName(String sPriorityName) {
		this.sPriorityName = sPriorityName;
	}

	@Override
	public String toString() {
		return "Priority [nPriorityId=" + nPriorityId + ", sPriorityName=" + sPriorityName + ", userPriorityMappings="
				+ userPriorityMappings + "]";
	}
}
