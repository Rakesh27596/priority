package com.user.priority.UserPriority.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="user_priority_mapping")
@NamedQuery(name="UserPriorityMapping.findAll", query="SELECT t from UserPriorityMapping t")
public class UserPriorityMapping {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_priority_mapping_id")
	private Integer nUserPriorityMappingId;
	
	@Column(name = "rate_of_satisfaction")
	private Integer nRateOfSatisfaction;
	
	@ManyToOne
	@JoinColumn(name="priority_id")
	private Priority priority;

	public Integer getnUserPriorityMappingId() {
		return nUserPriorityMappingId;
	}

	public void setnUserPriorityMappingId(Integer nUserPriorityMappingId) {
		this.nUserPriorityMappingId = nUserPriorityMappingId;
	}

	public Integer getnRateOfSatisfaction() {
		return nRateOfSatisfaction;
	}

	public void setnRateOfSatisfaction(Integer nRateOfSatisfaction) {
		this.nRateOfSatisfaction = nRateOfSatisfaction;
	}

	public Priority getPriority() {
		return priority;
	}

	public void setPriority(Priority priority) {
		this.priority = priority;
	}

	@Override
	public String toString() {
		return "UserPriorityMapping [nUserPriorityMappingId=" + nUserPriorityMappingId + ", nRateOfSatisfaction="
				+ nRateOfSatisfaction + ", priority=" + priority + "]";
	}
	
}
