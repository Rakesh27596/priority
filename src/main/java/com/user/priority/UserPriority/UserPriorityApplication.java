package com.user.priority.UserPriority;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserPriorityApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserPriorityApplication.class, args);
	}

}
